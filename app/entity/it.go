//角色”他、它，她“的功能模块
//author:  邵珠民
//start time:  2020-12-16 11:00:00
//end time:2020-12-16 11:00:00
package entity
type It struct {
	Entity
	thetype int32
	is_lift int8 //是否有生命 0未知，1是，2否
	is_person int8//是否是人 0未知，1是，2否
	is_foot int8 //是否是食物 0未知，1是，2否
}

//它it初始化
func InitializeIt(id string) (it It){
	it.Id                = id
	it.Name.ILikeName    = []ILikeNameData{{Name:"",Start: "",End: ""}}
	return it
}