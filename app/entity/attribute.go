package entity

type disease struct{
	Id string
	Name string
	Position string
	Probability int //概率 发生的几率
	IsInfect  bool //是否传染
	infectivity int //传染率
}
