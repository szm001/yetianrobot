package entity

import "fmt"
//年龄段字段结构体
type AgeGroups struct{
	Name string
	MinVal int
	MaxVal int
}
//年龄段
var AgeGroupsList []AgeGroups = []AgeGroups{
	{
		Name: "kid",
		MinVal: 0,
		MaxVal: 8,
	},
	{
		Name: "youth",
		MinVal: 9,
		MaxVal: 17,
	},
	{
		Name: "prime",
		MinVal: 18,
		MaxVal: 29,
	},
	{
		Name: "middleage",
		MinVal: 30,
		MaxVal: 59,
	},
	{
		Name: "oldage",
		MinVal: 60,
		MaxVal: 150,
	},
}
//心情字段结构体
type Mood struct{
	Name string
	MinMoodVal int //心情指数：0-29 痛苦悲伤 30-59 难过不乐  60-89：平常  90-100 高兴快乐
	MaxMoodVal int //心情指数：0-29 痛苦悲伤 30-59 难过不乐  60-89：平常  90-100 高兴快乐
}
//心情
var MoodList []Mood = []Mood{
	{
		Name: "happy",
		MinMoodVal: 90,
		MaxMoodVal: 100,
	},
	{
		Name: "common",
		MinMoodVal: 60,
		MaxMoodVal: 89,
	},
	{
		Name: "bad",
		MinMoodVal: 30,
		MaxMoodVal: 59,
	},
	{
		Name: "sad",
		MinMoodVal: 0,
		MaxMoodVal: 29,
	},
}

//危险系数结构体
type Dangerfactor struct{
	Name string
	MinMoodVal int //心情指数：0-29 痛苦悲伤 30-59 难过不乐  60-89：平常  90-100 高兴快乐
	MaxMoodVal int //心情指数：0-29 痛苦悲伤 30-59 难过不乐  60-89：平常  90-100 高兴快乐
}
//危险系数
var DangerfactorList []Dangerfactor = []Dangerfactor{
	{
		Name: "deadly",
		MinMoodVal: 90,
		MaxMoodVal: 100,
	},
	{
		Name: "deadly",
		MinMoodVal: 60,
		MaxMoodVal: 89,
	},
	{
		Name: "danger",
		MinMoodVal: 30,
		MaxMoodVal: 59,
	},
	{
		Name: "safe",
		MinMoodVal: 0,
		MaxMoodVal: 29,
	},
}
//我喜欢别人对我的称呼结构体
type ILikeNameData struct{
	Name string
	Start string
	End string
}
//名字结构体
type Name struct{
	OfficialName map[string]string
	Alias []map[string]string
	HeLikeName []map[string]string
	ILikeName []ILikeNameData
}


//=============================================
//实体结构体
type Entity struct {
	Id    string
	Name
	Color string
	Size  interface{}
	DangerfactorVal int //危险系数
	MoodVal int //心情系数
}

//角色的说话操作
func (o Entity) GetILikeName() string {
	if len(o.Name.ILikeName)>0{
		for _,k:=range o.Name.ILikeName{
			return k.Name
		}
	}
	return ""
}

//角色的说话操作
func (o Entity) Say(content string) {
	//内容打印到标准的输出设备
	fmt.Println(o.GetILikeName() +"说:你说的是《"+content+"》")
	//内容通过音响播放
	//o.Voice(role, content)
	//通过其他输入内容
}

//通过声音播放内容
func (o Entity) Voice(role string, content string) (ok error) {
	var err error
	return err
}

//通过视觉获得内容
func (o Entity) Look(role string) (content interface{}, ok error) {
	var err error
	return "", err
}





