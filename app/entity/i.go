// Package entity 角色”我“的功能模块
//author:  邵珠民
//start time:  2020-12-16 11:00:00
//end time:2020-12-16 11:00:00
package entity

import (
	"time"
)
type YouData struct {
	YouId string
	Familiarity int //熟悉程度 0-100
	Gender int  //行别
	AgeGroups string  //年龄段
	Mood string  //心情
	msgflat chan  []byte
}
type I struct {
	Person
	YouList []YouData
}
//{我}的初始化
func InitializeI(id,name string) (i I){
	i.Id                = "dddddd"
	i.Name.OfficialName = map[string]string{"name":name}
	i.Name.ILikeName    = []ILikeNameData{{Name:name,Start: "",End: ""}}
	i.BirthDate         = "2016-04-27"
	i.Say_ok            = false
	i.Gender            = 2
	i.AgeGroups         = "kid"  //年龄段
	i.Mood              = "happy"  //心情
	return i
}
//思考
func (i I) Thinking() (ret string){
	return "我在思考。。。"
}


func (i I) DealWith(theyou You,conteng_chan chan string) {
	var content string
	for {
		//i.Say("我在思考。。。")
		if i.Say_ok==false{
			//time.Sleep(1*time.Second)
			//continue
		}
		//是否认识
		if theyou.Is_know != 1{
			i.Say("你好！我是"+i.GetILikeName())
			i.Say("你是哪位？")
		}
		content = <- conteng_chan
		i.Say("<记住了你说的话："+content+">")
		theyou.Is_know = 1
		//i.Say_ok =false
		//theyou.Say_ok = true
		time.Sleep(1*time.Second)
	}

}

func (i I) Anwer(content string) (ret string){
	return content
}

// GetYouInfo 获得某一位的信息
func (i I) GetYouInfo(youid string) YouData {
	var ret YouData
	for _,v := range i.YouList{
		if v.YouId == youid{
			return v
		}
	}
	return ret
}


