//角色”你“的功能模块
//author:  邵珠民
//start time:  2020-12-16 11:00:00
//end time:2020-12-16 11:00:00
package entity

type You struct {
	Person
	Is_know int8 //是否认识，0未知，1认识，2不认识
}
//你的初始化
func InitializeYou(id string) (you You){
	you.Id                = id
	you.Name.ILikeName    = []ILikeNameData{{Name:"",Start: "",End: ""}}
	return you
}
func (p You) Anwer(content string) (ret string){
	return "没什么好说的"
}
