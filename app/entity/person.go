package entity

import (
	"strconv"
	"strings"
	"time"
)
//性别
var GenderType []int = []int{0,1,2,3,4,5} //0未知，1男，2女，3双性，4男转女，5女改男,6保密


type Person struct{
	Entity
	BirthDate string
	Say_ok bool
	Gender int  //行别
	AgeGroups string  //年龄段
	Mood string  //心情
}

func (p Person) Eat(o Entity) (ret string, err error){
	return "",err
}
func (p Person) Anwer(content string) (ret string){
	return "没什么好说的"
}
func (p Person) GetAge() (age int64){
	if p.BirthDate == "" {
		return 0
	}
	birthday := strings.Split(p.BirthDate,"-")

	if len(birthday) < 3 {
		return 0
	}

	birYear,_ := strconv.Atoi(birthday[0])
	birMonth,_ := strconv.Atoi(birthday[1])

	age_tmp := time.Now().Year() - birYear
	if age_tmp<=0{
		return 0
	}
	if int(time.Now().Month()) < birMonth {
		age_tmp--
	}
	age = int64(age_tmp)
	return age
}

func (p Person) Run() {
	//是否认识

	p.Say("你好！我是"+p.GetILikeName())
	p.Say("你是哪位？")
}
