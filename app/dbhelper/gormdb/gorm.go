package gormdb

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"os"
	//"gorm.io/gorm"
	"github.com/jinzhu/gorm"
	"log"
	dbconfig "robot/app/config/db"
	"sync"
)

/*
* MysqlConnectiPool
* 数据库连接操作库
* 基于gorm封装开发
 */
type MysqlConnectiPool struct {
}

var instance *MysqlConnectiPool
var once sync.Once

var db *gorm.DB
var err_db error

func GetInstance() *MysqlConnectiPool {
	once.Do(func() {
		instance = &MysqlConnectiPool{}
	})
	return instance
}

/*
* @fuc 初始化数据库连接(可在mail()适当位置调用)
 */
func (m *MysqlConnectiPool) InitDataPool() (issucc bool) {
	dns := dbconfig.User+":"+dbconfig.Password+"@tcp("+dbconfig.Host+":"+dbconfig.Port+")/"+dbconfig.Database
	//db, err_db = gorm.Open("mysql", "user:password@tcp(192.168.1.4:3306)/dbname?charset=utf8&parseTime=True&loc=Local")
	db, err_db = gorm.Open("mysql", dns)
	fmt.Println(err_db)
	if err_db != nil {
		log.Fatal(err_db)
		return false
	}
	//关闭数据库，db会被多个goroutine共享，可以不调用
	// defer db.Close()
	return true
}

/*
* @fuc  对外获取数据库连接对象db
 */
func (m *MysqlConnectiPool) GetMysqlDB() (db_con *gorm.DB) {
	issucc := m.InitDataPool()
	if !issucc {
		log.Println("init database pool failure...")
		os.Exit(1)
	}
	return db
}

var Db = GetInstance().GetMysqlDB()
