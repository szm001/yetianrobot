package model

import (
	"fmt"
	dbconfig "robot/app/config/db"
	"robot/app/dbhelper/gormdb"
)
type CompanyModel struct {
	Id int64 `gorm:"column:id"`
	Name string `gorm:"column:name"`
	Numbercode string `gorm:"column:numbercode"`
	Firstindustrycate string `gorm:"column:firstindustrycate"`
	Secondindustrycate string `gorm:"column:secondindustrycate"`
	Pqr int64 `gorm:"column:pqr"`//批次
	Sort int64 `gorm:"column:sort"`
	Note string `gorm:"column:note"`

}
func (model *CompanyModel) GetTableName() string {
	return dbconfig.TablePrefix + "nlp_company"
}
//查询数据是否已经添加
func (model *CompanyModel) Ishas(name string) int64 {
	var count int64
	db := gormdb.Db
	db = db.Table(model.GetTableName()).Where("name='"+name+"'").Count(&count)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return count
}
func (model *CompanyModel) Save() int64 {
	db := gormdb.Db
	//created_at := utils.GetDateTime()
	//这里使用了Table()函数，如果你没有指定全局表名禁用复数，或者是表名跟结构体名不一样的时候
	//你可以自己在sql中指定表名。这里是示例，本例中这个函数可以去除。
	db = db.Table(model.GetTableName()).Create(model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return model.Id
}
func (model *CompanyModel) Update(where string, args interface{}) int64 {
	//created_at := utils.GetDateTime()
	db := gormdb.Db.Table(model.GetTableName()).Where(where,args).Updates(&model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

// BatchSave 批量插入数据
func (model *CompanyModel) BatchSave(datalist []*CompanyModel) int64 {
	var sql string
	db := gormdb.Db
	sql = "insert into `"+model.GetTableName()+"` (" +
		"name," +
		"numbercode," +
		"firstindustrycate," +
		"secondindustrycate," +
		"pqr," +
		"sort," +
		"note" + ") values"
	for i, e := range datalist {
		if i == len(datalist)-1 {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s',%d,%d,'%s');",
				e.Name,
				e.Numbercode,
				e.Firstindustrycate,
				e.Secondindustrycate,
				e.Pqr,
				e.Sort,
				e.Note,
				)
		} else {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s',%d,%d,'%s'),",
				e.Name,
				e.Numbercode,
				e.Firstindustrycate,
				e.Secondindustrycate,
				e.Pqr,
				e.Sort,
				e.Note,
			)
		}
	}
	db = db.Exec(sql)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

