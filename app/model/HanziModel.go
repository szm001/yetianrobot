package model

import (
	"fmt"
	dbconfig "robot/app/config/db"
	"robot/app/dbhelper/gormdb"
)
type HanziModel struct {
	Id int64 `gorm:"column:id"`
	Word string `gorm:"column:word"`
	Word_code string `gorm:"column:word_code"`
	Pos string `gorm:"column:pos"`
	Initial string `gorm:"column:initial"`
	Percent float32 `gorm:"column:percent"`
	Strokes int64 `gorm:"column:strokes"`
	Sort int64 `gorm:"column:sort"`
	Note string `gorm:"column:note"`

}
func (model *HanziModel) GetTableName() string {
	return dbconfig.TablePrefix + "nlp_hanzi"
}
//查询数据是否已经添加
func (model *HanziModel) Ishas(word_code string) int64 {
	var count int64
	db := gormdb.Db
	db = db.Table(model.GetTableName()).Where("word_code='"+word_code+"'").Count(&count)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return count
}
func (model *HanziModel) Save() int64 {
	db := gormdb.Db
	//created_at := utils.GetDateTime()
	//这里使用了Table()函数，如果你没有指定全局表名禁用复数，或者是表名跟结构体名不一样的时候
	//你可以自己在sql中指定表名。这里是示例，本例中这个函数可以去除。
	db = db.Table(model.GetTableName()).Create(model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return model.Id
}
func (model *HanziModel) Update(where string, args interface{}) int64 {
	//created_at := utils.GetDateTime()
	db := gormdb.Db.Table(model.GetTableName()).Where(where,args).Updates(&model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

// BatchSave 批量插入数据
func (model *HanziModel) BatchSave(datalist []*HanziModel) int64 {
	var sql string
	db := gormdb.Db
	sql = "insert into `"+model.GetTableName()+"` (" +
		"word," +
		"word_code," +
		"initial," +
		"pos," +
		"percent," +
		"strokes," +
		"sort," +
		"note" + ") values"
	for i, e := range datalist {
		if i == len(datalist)-1 {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s',%f,%d,%d,'%s');",
				e.Word,
				e.Word_code,
				e.Initial,
				e.Pos,
				e.Percent,
				e.Strokes,
				e.Sort,
				e.Note,
				)
		} else {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s',%f,%d,%d,'%s'),",
				e.Word,
				e.Word_code,
				e.Initial,
				e.Pos,
				e.Percent,
				e.Strokes,
				e.Sort,
				e.Note,
			)
		}
	}
	db = db.Exec(sql)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

