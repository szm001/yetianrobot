package model

import (
	"fmt"
	dbconfig "robot/app/config/db"
	"robot/app/dbhelper/gormdb"
)
type PosModel struct {
	Id int64 `gorm:"column:id"`
	Word string `gorm:"column:word"`
	Name string `gorm:"column:name"`
	Pcode string `gorm:"column:pcode"`
	Sort int64 `gorm:"column:sort"`
	Note string `gorm:"column:note"`

}
func (model *PosModel) GetTableName() string {
	return dbconfig.TablePrefix + "nlp_pos"
}
//查询数据是否已经添加
func (model *PosModel) Ishas(word string) int64 {
	var count int64
	db := gormdb.Db
	db = db.Table(model.GetTableName()).Where("word='"+word+"'").Count(&count)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return count
}

//查询词性的上下级列表
func (model *PosModel) GetPosList(word string) []string {
	db := gormdb.Db
	var ret []string
	var data PosModel = PosModel{}
	db = db.Table(model.GetTableName()).Where("word='"+word+"'").First(&data)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return ret
	}
	ret1 := data.Word
	if data.Pcode != ""{
		ret = model.GetPosList(data.Pcode)
	}
	return append(ret, ret1)
}

func (model *PosModel) Save() int64 {
	db := gormdb.Db
	//created_at := utils.GetDateTime()
	//这里使用了Table()函数，如果你没有指定全局表名禁用复数，或者是表名跟结构体名不一样的时候
	//你可以自己在sql中指定表名。这里是示例，本例中这个函数可以去除。
	db = db.Table(model.GetTableName()).Create(model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return model.Id
}
func (model *PosModel) Update(where string, args interface{}) int64 {
	//created_at := utils.GetDateTime()
	db := gormdb.Db.Table(model.GetTableName()).Where(where,args).Updates(&model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

// BatchSave 批量插入数据
func (model *PosModel) BatchSave(wordverblist []*PosModel) int64 {
	var sql string
	db := gormdb.Db
	sql = "insert into `"+model.GetTableName()+"` (" +
		"word," +
	"percent," +
	"sort," +
	"note" + ") values"
	for i, e := range wordverblist {
		if i == len(wordverblist)-1 {
			sql = sql+ fmt.Sprintf("('%s','%s','%s',%d,'%s');",
				e.Word,
				e.Name,
				e.Pcode,
				e.Sort,
				e.Note,
				)
		} else {
			sql = sql+ fmt.Sprintf("('%s','%s','%s',%d,'%s'),",
				e.Word,
				e.Name,
				e.Pcode,
				e.Sort,
				e.Note,
			)
		}
	}
	db = db.Exec(sql)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

