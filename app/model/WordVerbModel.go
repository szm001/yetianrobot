package model

import (
	"fmt"
	dbconfig "robot/app/config/db"
	"robot/app/dbhelper/gormdb"
	"strconv"
)
type WordVerb struct {
	Id int64 `gorm:"column:id"`
	Token string `gorm:"column:token"` //名词md5
	Name string `gorm:"column:name"` //动词名称
	Pinyin string `gorm:"column:pinyin"` //拼音
	Jianlei string `gorm:"column:jianlei"` //兼类类型
	Hougen string `gorm:"column:hougen"` //后跟字词
	Lihe string `gorm:"column:lihe"` //离合动词
	Justbebuyu int `gorm:"column:justbebuyu"` //只做补语0不1是
	Justbezhuangyu int `gorm:"column:justbezhuangyu"` //只做状语0不1是
	Justbeweiyu int `gorm:"column:justbeweiyu"` //只做谓语0不1是
	A_a int `gorm:"column:a_a"` //重叠a一a式0不1是,如帮一帮
	Aab int `gorm:"column:aab"` //重叠aab式0不1是，如散散步
	Aabb int `gorm:"column:aabb"` //重叠aabb式0不1是
	Abab int `gorm:"column:abab"` //重叠abab式0不1是
	Zaizhengzai int `gorm:"column:zaizhengzai"` //在正在0不1是
	Zhe int `gorm:"column:zhe"` //着0不1是
	Guo int `gorm:"column:guo"` //过0不1是
	Le int `gorm:"column:le"` //了0不1是
	Hen int `gorm:"column:hen"` //很0不1是
	Mei int `gorm:"column:mei"` //没0不1是
	Bu int `gorm:"column:bu"` //不0不1是
	Dongqu int `gorm:"column:dongqu"` //动驱0不1是
	Jianyuju int `gorm:"column:jianyuju"` //兼语句0不1是
	Shuangbin int `gorm:"column:shuangbin"` //双宾0不1是
	Jubin int `gorm:"column:jubin"` //句宾动词0不1是
	Zhunbin int `gorm:"column:zhunbin"` //准宾动词0不1是
	Weibin int `gorm:"column:weibin"` //渭宾动词0不1是
	Tibin int `gorm:"column:tibin"` //体宾动词0不1是
	Jiwu int `gorm:"column:jiwu"` //及物动词（外动词）0不1是
	Bujiwu int `gorm:"column:bujiwu"` //不及物动词（外动词）0不1是
	Houming int `gorm:"column:houming"` //后名0不1
	Qianming int `gorm:"column:qianming"` //前名0不1是
	Youbinyu int `gorm:"column:youbinyu"` //有宾语0不1是
	Tongxing int `gorm:"column:tongxing"` //同形
	Xici int `gorm:"column:xici"` //系词：0不是，1是
	Zhudongci int `gorm:"column:Zhudongci"` //助动词0不1是
	Quxiangdongci int `gorm:"column:quxiangdongci"` //趋向动词0不1是
	Xingshidongci int `gorm:"column:xingshidongci"` //形式动词0不1是
	Zhunweibin int `gorm:"column:zhunweibin"` //准渭宾0不1是
	Note string `gorm:"column:note"` //备注
}
func (model *WordVerb) GetTableName() string {
	return dbconfig.TablePrefix + "nlp_word_verb"
}
func (model *WordVerb) Save() int64 {
	db := gormdb.Db
	//created_at := utils.GetDateTime()
	//这里使用了Table()函数，如果你没有指定全局表名禁用复数，或者是表名跟结构体名不一样的时候
	//你可以自己在sql中指定表名。这里是示例，本例中这个函数可以去除。
	db = db.Table(model.GetTableName()).Create(model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return model.Id
}

func (model *WordVerb) update(where interface{}, args interface{}) int64 {
	//created_at := utils.GetDateTime()
	db := gormdb.Db.Table(model.GetTableName()).Where(where,args).Update(&model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}
// BatchSave 批量插入数据
func (model *WordVerb) BatchSave(wordverblist []*WordVerb) int64 {
	var sql string
	db := gormdb.Db
	sql = "insert into `le_nlp_word_verb` (" +
		"token," +
	"name," +
	"pinyin," +
	"jianlei," +
	"hougen," +
	"lihe," +
	"justbebuyu," +
	"justbezhuangyu," +
	"justbeweiyu," +
	"a_a," +
	"aab," +
	"aabb," +
	"abab," +
	"zaizhengzai," +
	"zhe," +
	"guo," +
	"le," +
	"hen," +
	"mei," +
	"bu," +
	"dongqu," +
	"jianyuju," +
	"shuangbin," +
	"jubin," +
	"zhunbin," +
	"weibin," +
	"tibin," +
	"jiwu," +
	"bujiwu," +
	"houming," +
	"qianming," +
	"youbinyu," +
	"tongxing," +
	"xici," +
	"Zhudongci," +
	"quxiangdongci," +
	"xingshidongci," +
	"zhunweibin," +
	"note" + ") values"
	for i, e := range wordverblist {
		if i == len(wordverblist)-1 {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s');",
				e.Token,
				e.Name,
				e.Pinyin,
				e.Jianlei,
				e.Hougen,
				e.Lihe,
				e.Justbebuyu,
				e.Justbezhuangyu,
				e.Justbeweiyu,
				e.A_a,
				e.Aab,
				e.Aabb,
				e.Abab,
				e.Zaizhengzai,
				e.Zhe,
				e.Guo,
				e.Le,
				e.Hen,
				e.Mei,
				e.Bu,
				e.Dongqu,
				e.Jianyuju,
				e.Shuangbin,
				e.Jubin,
				e.Zhunbin,
				e.Weibin,
				e.Tibin,
				e.Jiwu,
				e.Bujiwu,
				e.Houming,
				e.Qianming,
				e.Youbinyu,
				e.Tongxing,
				e.Xici,
				e.Zhudongci,
				e.Quxiangdongci,
				e.Xingshidongci,
				e.Zhunweibin,
				e.Note,)
		} else {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s','%s','%s',%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s'),",
			e.Token,
			e.Name,
			e.Pinyin,
			e.Jianlei,
			e.Hougen,
			e.Lihe,
			e.Justbebuyu,
			e.Justbezhuangyu,
			e.Justbeweiyu,
			e.A_a,
			e.Aab,
			e.Aabb,
			e.Abab,
			e.Zaizhengzai,
			e.Zhe,
			e.Guo,
			e.Le,
			e.Hen,
			e.Mei,
			e.Bu,
			e.Dongqu,
			e.Jianyuju,
			e.Shuangbin,
			e.Jubin,
			e.Zhunbin,
			e.Weibin,
			e.Tibin,
			e.Jiwu,
			e.Bujiwu,
			e.Houming,
			e.Qianming,
			e.Youbinyu,
			e.Tongxing,
			e.Xici,
			e.Zhudongci,
			e.Quxiangdongci,
			e.Xingshidongci,
			e.Zhunweibin,
			e.Note,
			)
		}
	}
	db = db.Exec(sql)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

func (model *WordVerb) QueryById(id int) WordVerb {
	u := WordVerb{}
	db := gormdb.Db
	//db = db.Table(model.GetTableName()).Select("token,name").Where(where,args).First(&model)
	db = db.Table(model.GetTableName()).Select("*").Where("id=?",id).First(&u)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return u
	}
	return u
}
//统计总记录数
func (model *WordVerb) Count(where interface{}, args interface{}) int64 {
	var count int64
	db := gormdb.Db
	db = db.Table(model.GetTableName()).Where(where,args).Count(&count)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return count
}
//查询列表，翻页列表
func  (model *WordVerb) GetList(args map[string]string) ( []WordVerb, error) {
	db := gormdb.Db
	db = db.Table(model.GetTableName())
	if _, ok := args["page"]; !ok {
		args["page"] = "1"
	}
	if _, ok := args["pageSize"]; !ok {
		args["pageSize"] = "10"
	}
	if keywords, ok := args["keywords"]; ok {
		//db = db.Where("page = ?", "1")
		db = db.Where("title like ?", "%"+keywords+"%")
	}
	page, _ := strconv.Atoi(args["page"])
	pageSize, _ := strconv.Atoi(args["pageSize"])
	if page > 0 && pageSize > 0 {
		db = db.Limit(pageSize).Offset((page - 1) * pageSize)
	}
	var datalist []WordVerb
	if err1 := db.Find(&datalist).Error; err1 != nil {
		fmt.Println(err1.Error())
		return datalist,err1
	}
	return datalist,nil
}
//删除
func (model *WordVerb) Del(where interface{}, args ...interface{}) int64 {
	db := gormdb.Db
	db = db.Table(model.GetTableName())
	db = db.Where(where,args).Delete(&model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}
