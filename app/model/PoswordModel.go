package model

import (
	"fmt"
	dbconfig "robot/app/config/db"
	"robot/app/dbhelper/gormdb"
)
type PoswordModel struct {
	Id int64 `gorm:"column:id"`
	Language string `gorm:"column:language"` //语言
	Dialect string `gorm:"column:dialect"` //方言
	Word string `gorm:"column:word"` //字/词
	Initial string `gorm:"column:initial"` //首字母
	Percent float32 `gorm:"column:percent"` //概率
	Pos_first_cate string `gorm:"column:pos_first_cate"` //词性1级
	Pos_second_cate string `gorm:"column:pos_second_cate"` //词性2级
	Pos_third_cate string `gorm:"column:pos_third_cate"` //词性3级
	Pos_fourth_cate string `gorm:"column:pos_fourth_cate"` //词性4级
	Nation string `gorm:"column:nation"` //民族
	Religion string `gorm:"column:religion"` //宗教
	Country string `gorm:"column:country"` //国家
	Province string `gorm:"column:province"` //省/州
	Region string `gorm:"column:region"` //地区
	County string `gorm:"column:county"` //郡/县
	Town string `gorm:"column:town"` //乡镇
	Village string `gorm:"column:village"` //村，街道办
	Situation string `gorm:"column:situation"` //场合：婚礼，官场，战场，家里，公共
	Situation_status string `gorm:"column:situation_status"` //场合状态：未开始，进行中，已结束
	Times string `gorm:"column:times"` //时代：古代，近代，现代
	Gender string `gorm:"column:gender"` //性别：男，女，通用
	Agegroups string `gorm:"column:agegroups"` //年龄段 幼儿，少年，青年，中年，老年
	Marriage string `gorm:"column:marriage"` //婚否：已婚 未婚，离异',
	Target string `gorm:"column:target"` //对象：丈夫，亲人，兄弟，姐妹，陌生人，敌人，朋友，同事，同伙，爱人，上司，儿女，下级，长辈，晚辈
	Emotion string `gorm:"column:emotion"` //感情情感情绪：傲慢，谦虚，看不起，自卑
	Occupation string `gorm:"column:occupation"` //职业或身份：学生，教师，律师，商业，企业主，军人，医生，小偷，逃兵，杀人犯，教父，村长，作家
	Sort int64 `gorm:"column:sort"`
	Status int64 `gorm:"column:status"` //状态:0可用1不可用
	Attribute string `gorm:"column:attribute"` //属性：json串
	Note string `gorm:"column:note"`

}
func (model *PoswordModel) GetTableName() string {
	return dbconfig.TablePrefix + "nlp_pos_word"
}
//查询数据是否已经添加
func (model *PoswordModel) Ishas(word string) int64 {
	var count int64
	db := gormdb.Db
	db = db.Table(model.GetTableName()).Where("word='"+word+"'").Count(&count)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return count
}

func (model *PoswordModel) Save() int64 {
	db := gormdb.Db
	//created_at := utils.GetDateTime()
	//这里使用了Table()函数，如果你没有指定全局表名禁用复数，或者是表名跟结构体名不一样的时候
	//你可以自己在sql中指定表名。这里是示例，本例中这个函数可以去除。
	db = db.Table(model.GetTableName()).Create(model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return model.Id
}
func (model *PoswordModel) Update(where string, args interface{}) int64 {
	//created_at := utils.GetDateTime()
	db := gormdb.Db.Table(model.GetTableName()).Where(where,args).Updates(&model)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

// BatchSave 批量插入数据
func (model *PoswordModel) BatchSave(wordverblist []*PoswordModel) int64 {
	var sql string
	db := gormdb.Db
	sql = "insert into `"+model.GetTableName()+"` (" +
	"language," + //语言
	"dialect," + //方言
	"word," + //字/词
	"initial," + //首字母
	"percent," + //概率
	"pos_first_cate," + //词性1级
	"pos_second_cate," + //词性2级
	"pos_third_cate," + //词性3级
	"pos_fourth_cate," + //词性4级
	"nation," + //民族
	"religion," + //宗教
	"country," + //国家
	"province," + //省/州
	"region," + //地区
	"county," + //郡/县
	"town," + //乡镇
	"village," + //村，街道办
	"situation," + //场合：婚礼，官场，战场，家里，公共
	"situation_status," + //场合状态：未开始，进行中，已结束
	"times," + //时代：古代，近代，现代
	"gender," + //性别：男，女，通用
	"agegroups," + //年龄段 幼儿，少年，青年，中年，老年
	"marriage," + //婚否：已婚 未婚，离异',
	"target," + //对象：丈夫，亲人，兄弟，姐妹，陌生人，敌人，朋友，同事，同伙，爱人，上司，儿女，下级，长辈，晚辈
	"emotion," + //感情情感情绪：傲慢，谦虚，看不起，自卑
	"occupation," + //职业或身份：学生，教师，律师，商业，企业主，军人，医生，小偷，逃兵，杀人犯，教父，村长，作家
	"sort," +
	"status," + //状态:0可用1不可用
	"attribute," + //属性：json串
	"note" + ") values"
	for i, e := range wordverblist {
		if i == len(wordverblist)-1 {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s',%f,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,%d,'%s','%s');",
				e.Language,
				e.Dialect,
				e.Word,
				e.Initial,
				e.Percent,
				e.Pos_first_cate,
				e.Pos_second_cate,
				e.Pos_third_cate,
				e.Pos_fourth_cate,
				e.Nation,
				e.Religion,
				e.Country,
				e.Province,
				e.Region,
				e.County,
				e.Town,
				e.Village,
				e.Situation,
				e.Situation_status,
				e.Times,
				e.Gender,
				e.Agegroups,
				e.Marriage,
				e.Target,
				e.Emotion,
				e.Occupation,
				e.Sort,
				e.Status,
				e.Attribute,
				e.Note,
				)
		} else {
			sql = sql+ fmt.Sprintf("('%s','%s','%s','%s',%f,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,%d,'%s','%s'),",
				e.Language,
				e.Dialect,
				e.Word,
				e.Initial,
				e.Percent,
				e.Pos_first_cate,
				e.Pos_second_cate,
				e.Pos_third_cate,
				e.Pos_fourth_cate,
				e.Nation,
				e.Religion,
				e.Country,
				e.Province,
				e.Region,
				e.County,
				e.Town,
				e.Village,
				e.Situation,
				e.Situation_status,
				e.Times,
				e.Gender,
				e.Agegroups,
				e.Marriage,
				e.Target,
				e.Emotion,
				e.Occupation,
				e.Sort,
				e.Status,
				e.Attribute,
				e.Note,
			)
		}
	}
	db = db.Exec(sql)
	if db.Error !=nil{
		fmt.Println(db.Error)
		return 0
	}
	return db.RowsAffected
}

