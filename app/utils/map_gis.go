package utils

import (
	"fmt"
	"math"
)

func GisTest() {
	//上海坐标
	lat1 := 31.22
	lng1 := 121.48
	//上海宝山
	//lat3 := 31.41
	//lng3 := 121.48
	//广东坐标
	lat2 := 23.16
	lng2 := 113.23
	//北京
	lat21 := 39.92
	lng21 := 116.46
	fmt.Println(Getdistance(lat1, lng1, lat2, lng2))
	fmt.Println(Getdistance(lat1, lng1, lat21, lng21))
	//fmt.Println(GetDirection(lat1, lng1, lat2, lng2))
}
//求2个坐标的距离，单位为米
func Getdistance(lat1, lon1, lat2, lon2 float64)float64 {
	radius := float64(6371000)
	rad :=math.Pi/180.0
	lat1 = lat1 * rad
	lon1 = lon1 * rad
	lat2 = lat2 * rad
	lon2 = lon2 * rad
	theta := lon2 - lon1
	dist := math.Acos(math.Sin(lat1) * math.Sin(lat2) + math.Cos(lat1) * math.Cos(lat2) * math.Cos(theta))
	return dist * radius * 1000
}
//求2个坐标的方位角
func GetAngle(lat1, lng1, lat2,lng2 float64) float64 {
	x1 := lng1
	y1 := lat1
	x2 := lng2
	y2 := lat2
	pi := math.Pi
	w1 := y1 / 180 * pi
	j1 := x1 / 180 * pi
	w2 := y2 / 180 * pi
	j2 := x2 / 180 * pi
	if j1 == j2 {
		if w1 > w2{
			return float64(270) // 北半球的情况，南半球忽略
		}else if w1 < w2{
			return 90;
		}else{
			return -1// 位置完全相同
		}
	}
	ret := 4* math.Pow(math.Sin((w1 - w2) / 2), 2)- math.Pow(math.Sin((j1 - j2) / 2) * (math.Cos(w1) - math.Cos(w2)),2)
	ret = math.Sqrt(ret)
	temp := math.Sin(math.Abs(j1 - j2) / 2) * (math.Cos(w1) + math.Cos(w2))
	ret = ret / temp
	ret = math.Atan(ret) / pi * 180
	if j1 > j2{ // 1为参考点坐标
		if w1 > w2{
			ret += 180
		} else{
			ret = 180 - ret
		}
	} else if w1 > w2{
		ret = 360 - ret
	}
	return ret
}
//求2个坐标的方位
func GetDirection(lat1, lng1, lat2,lng2 float64) string {
	jiaodu := GetAngle(lat1, lng1, lat2, lng2)
	if jiaodu <= 10 || jiaodu > 350{
		return "东"
	}
	if jiaodu > 10 && jiaodu <= 80{
		return "东北"
	}
	if jiaodu > 80 && jiaodu <= 100{
		return "北"
	}
	if jiaodu > 100 && jiaodu <= 170{
		return "西北"
	}
	if jiaodu > 170 && jiaodu <= 190{
		return "西"
	}
	if jiaodu > 190 && jiaodu <= 260{
		return "西南"
	}
	if jiaodu > 260 && jiaodu <= 280{
		return "南"
	}
	if jiaodu > 280 && jiaodu <= 350{
		return "东南"
	}
	return ""
}

