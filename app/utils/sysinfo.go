package utils

import (
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
	_ "github.com/shirou/gopsutil/process"
	"os"
	"runtime"
	"strconv"
)

func Getsysinfo() map[string]string{
	evnsys := map[string]string{}
	evnsys["os"] = os.Getenv("os")
	//fmtstudy.Println(os.Args)
	// 获取host name
	evnsys["hostname"],_ = os.Hostname()
	//fmt.Println(os.Getpid())
	// 获取全部环境变量
	//env := os.Environ()
	//for k, v := range env {
	//	fmt.Println(k, v)
	//}
	// 终止程序
	// os.Exit(1)
	// 获取一条环境变量
	evnsys["path"] = os.Getenv("PATH")
	// 获取当前目录
	evnsys["pwd"],_ = os.Getwd()
	// 创建目录
	//err = os.Mkdir(dir+"/new_file", 0755)
	// 创建目录
	//err = os.MkdirAll(dir+"/new", 0755)

	//var Osname = runtime.GOOS
	evnsys["arch"] = runtime.GOARCH  //386、amd64或arm
	//Win7 64bit系统：
	//windows
	//amd64

	//macOS(10.13.4) 64bit系统：
	//darwin
	//amd64

	//设置可同时执行的最大CPU数，并返回先前的设置：
	evnsys["usedcpunum"] = strconv.Itoa(runtime.GOMAXPROCS(0)) //4核机器返回：4
	evnsys["cpunum"] = strconv.Itoa(runtime.NumCPU()) //本地机器的逻辑CPU个数
	return evnsys
}


func Gethostinfo() *host.InfoStat {
	info, _ := host.Info()
	//output：
	//{
	//"hostname":"WIN-SP09TQCP1U8",
	//"uptime":25308,
	//"bootTime":1558574107,
	//"procs":175,
	//"os":"windows",
	//"platform":"Microsoft Windows 10 Pro",
	//"platformFamily":"Standalone Workstation",
	//"platformVersion":"10.0.17134 Build 17134",
	//"kernelVersion":"","virtualizationSystem":"",
	//"virtualizationRole":"","hostid":
	//}
	return info
}

func Getsysdata(){
	info, _ := cpu.Info()  //总体信息
	if info == nil {

	}
	//output：
	//[{"cpu":0,cores":4,"modelName":"Intel(R) Core(TM) i5-2520M CPU @ 2.50GHz","mhz":2501,。。。]
	c, _ := cpu.Counts(true)  //cpu逻辑数量
	c, _ = cpu.Counts(false)  //cpu物理核心 如果是2说明是双核超线程, 如果是4则是4核非超线程
	if c == 1 {

	}
	// 用户CPU时间/系统CPU时间/空闲时间。。。等等
	info2, _ := cpu.Times(false)

		//output：
	//[{"cpu":"cpu-total","user":1272.0,"system":1572.7,"idle":23092.3,"nice":0.0,"iowait":0.0,"irq":0.0,。。。}]
	//用户CPU时间：就是用户的进程获得了CPU资源以后，在用户态执行的时间。
	//系统CPU时间：用户进程获得了CPU资源以后，在内核态的执行时间。
	//CPU使用率，每秒刷新一次

	//for{
		//info, _ := cpu.Percent(time.Duration(time.Second), false)
	//}


	//获取内存信息
	//获取物理内存和交换区内存信息

	info3, _ := mem.VirtualMemory()

	info4, _ := mem.SwapMemory()
	//output：
	//{"total":8129818624,"available":4193423360,"used":3936395264,"usedPercent":48,"free":0,"active":0,"inactive":0,...}
	//{"total":8666689536,"used":4716843008,"free":3949846528,"usedPercent":0.5442496801583825,"sin":0,"sout":0,...}
	//总内存大小是8129818624 = 8 GB，已用3936395264 = 3.9 GB，使用了48%。而交换区大小是8666689536 = 8 GB。

	//获取磁盘信息
	//可以通过psutil获取磁盘分区、磁盘使用率和磁盘IO信息：

	info5, _ := disk.Partitions(true)  //所有分区
	info7, _ := disk.Usage("E:")  //指定某路径的硬盘使用情况
	info6, _ := disk.IOCounters()  //所有硬盘的io信息
	//output：
	//[{"device":"C:","mountpoint":"C:","fstype":"NTFS","opts":"rw.compress"} {"device":"D:","mountpoint":"D:","fstype":"NTFS","opts":"rw.compress"} {"device":"E:","mountpoint":"E:","fstype":"NTFS","opts":"rw.compress"} ]
	//{"path":"E:","fstype":"","total":107380965376,"free":46790828032,"used":60590137344,"usedPercent":56.425398236866755,"inodesTotal":0,"inodesUsed":0,"inodesFree":0,"inodesUsedPercent":0}
	//map[C::{"readCount":0,"mergedReadCount":0,"writeCount":0,"mergedWriteCount":0,"readBytes":0,"writeBytes":4096,"readTime":0,"writeTime":0,"iopsInProgress":0,"ioTime":0,"weightedIO":0,"name":"C:","serialNumber":"","label":""} 。。。]
	//获取网络信息
	//获取当前网络连接信息

	info8, _ := net.Connections("tcp")  //可填入tcp、udp、tcp4、udp4等等
	//output：
	//[{"fd":0,"family":2,"type":1,"localaddr":{"ip":"0.0.0.0","port":135},"remoteaddr":{"ip":"0.0.0.0","port":0},"status":"LISTEN","uids":null,"pid":668} {"fd":0,"family":2,"type":1,"localaddr":{"ip":"0.0.0.0","port":445},"remoteaddr":{"ip":"0.0.0.0","port":0},"status":"LISTEN","uids":null,"pid":4} {"fd":0,"family":2,"type":1,"localaddr":{"ip":"0.0.0.0","port":1801},"remoteaddr":{"ip":"0.0.0.0","port":0},"status":"LISTEN","uids":null,"pid":3860}
	//等等。。。]
	//获取网络读写字节／包的个数

	//info, _ := net.IOCounters(false)
	//output：[{"name":"all","bytesSent":6516450,"bytesRecv":36991210,"packetsSent":21767,"packetsRecv":33990,"errin":0,"errout":0,"dropin":0,"dropout":0,"fifoin":0,"fifoout":0}]
	//获取进程信息
	//获取到所有进程的详细信息：

	//info, _ := process.Pids()  //获取当前所有进程的pid
	//output：
	//[0 4 96 464 636 740 748 816 852 880 976 348 564 668 912 1048 1120 1184 1268 1288。。。]
	//info2,_ = process.GetWin32Proc(1120)  //对应pid的进程信息
	//output：
	//[{svchost.exe 0xc00003e570 0xc00003e580 8 2019-05-23 09:15:28.444192 +0800 CST 5600 4 0xc00003e5b0 0 0 0 0 Win32_ComputerSystem WIN-SE89TTCP7U3 0xc00003e620 Win32_Process 0xc00003e650 0xc00005331e 209 6250000 0xc000053378 0xc0000533b8 Win32_OperatingSystem Microsoft Windows 10 专业版。。。}]
	//fmt.Println(info2[0].ParentProcessID)  //获取父进程的pid


	_ = info2
	_ = info3
	_ = info4
	_ = info5
	_ = info6
	_ = info7
	_ = info8
}

func GetEthernetInfo() {
	//baseNicPath := "/sys/class/net/"
	//cmd1 := exec.Command("ls", baseNicPath)
	//buf, err := cmd1.Output()
	//if err != nil {
	//	//fmt.Println("Error:", err)
	//	return
	//}
	//output := string(buf)
	//
	//for _, device := range strings.Split(output, "\n") {
	//	if len(device) > 1 {
	//		ethHandle, err := ethtool.NewEthtool()
	//		if err != nil {
	//			panic(err.Error())
	//		}
	//		defer ethHandle.Close()
	//		stats, err := ethHandle.LinkState(device)
	//		if err != nil {
	//			panic(err.Error())
	//		}
	//		_=stats
	//		//fmt.Printf("LinkName: %s LinkState: %d\n", device, stats)
	//	}
	//
	//}

}



