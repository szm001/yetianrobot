package utils

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"github.com/jmcvetta/randutil"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

//字符串转单字符列表
func Stringtolist(str string) []string {
	bytelistw := []rune(str)
	var strlist []string
	for i:=0;i<len(bytelistw);i++{
		strlist = append(strlist,string(bytelistw[i]))
	}
	return strlist
}

func RootPath() string {
	s, err := exec.LookPath(os.Args[0])
	if err != nil {
		log.Panicln("发生错误", err.Error())
	}
	i := strings.LastIndex(s, "\\")
	path := s[0 : i+1]
	return path
}
//获得根目录
func GetRootPath() string {
	sep := GetPathSep()
	path, _:= os.Getwd()
	//path = filepath.Join(path,sep)
	path += sep
	return path
}

//获得根url
func GetRootUrl() string {
	return "/"
}

//获得域名
func GetDomain() string {
	return "127.0.0.1"
}

//返回datatime
func GetDateTime2()(formatTimeStr string){
	NowTimeZone := time.FixedZone("CST", 8*3600) //东八区，返回时区指针
	formatTimeStr =time.Now().In(NowTimeZone).Format("2006-01-02 15:04:05")   //2020-04-28 23:27:50
	return formatTimeStr
}


func GetDateTime() string {
	now := time.Now()
	str := Format("Y-m-d H:i:s", now)
	return str
}
//获得上传根目录
func GetUploadPath() string {
	sep := GetPathSep()
	path, _:= os.Getwd()
	path = filepath.Join(path,"upload")
	path += sep
	return path
}
//获得上传根目录
func GetDataPath() string {
	sep := GetPathSep()
	path, _:= os.Getwd()
	path = filepath.Join(path,"static"+sep+"data"+sep)
	path += sep
	return path
}

func GetPathSep() string {
	sep := string(os.PathSeparator)
	return sep
}
//int32转字符串 方法1
func StrconvInt32toString(n int32) string {
	return strconv.FormatInt(int64(n),10)
}
//int32转字符串 方法2
func Int32toString(n int32) string {
	buf := [11]byte{}
	pos := len(buf)
	i := int64(n)
	signed := i < 0
	if signed {
		i = -i
	}
	for {
		pos--
		buf[pos],i = '0'+byte(i%10),i/10
		if i == 0 {
			if signed {
				pos--
				buf[pos] = '-'
			}
			return string(buf[pos:])
		}
	}
}

//返回远程客户端的 IP，如 192.168.1.1
func Getip(req *http.Request) string {
	remoteAddr := req.RemoteAddr
	if ip := req.Header.Get("X-Real-IP"); ip != "" {
		remoteAddr = ip
	} else if ip = req.Header.Get("X-Forwarded-For"); ip != "" {
		remoteAddr = ip
	} else {
		remoteAddr, _, _ = net.SplitHostPort(remoteAddr)
	}

	if remoteAddr == "::1" {
		remoteAddr = "127.0.0.1"
	}

	return remoteAddr
}

// RandString 产生随机字符串，可用于密码等
func RandString(n int, defaultCharsets ...string) string {
	charset :=  randutil.Alphanumeric + "!%@#"
	if len(defaultCharsets) > 0 {
		charset = defaultCharsets[0]
	}

	result, err := randutil.String(n, charset)
	if err != nil {
		//fmt.Println("goutils RandString error:", err)
		//return Md5(strconv.Itoa(rand.Intn(999999)))
	}

	return result
}



func Md5(text string) string {
	hashMd5 := md5.New()
	io.WriteString(hashMd5, text)
	return fmt.Sprintf("%x", hashMd5.Sum(nil))
}

func Md5Buf(buf []byte) string {
	hashMd5 := md5.New()
	hashMd5.Write(buf)
	return fmt.Sprintf("%x", hashMd5.Sum(nil))
}

func Md5File(reader io.Reader) string {
	var buf = make([]byte, 4096)
	hashMd5 := md5.New()
	for {
		n, err := reader.Read(buf)
		if err == io.EOF && n == 0 {
			break
		}
		if err != nil && err != io.EOF {
			break
		}

		hashMd5.Write(buf[:n])
	}

	return fmt.Sprintf("%x", hashMd5.Sum(nil))
}

func Base64Encode(data string) string {
	return base64.URLEncoding.EncodeToString([]byte(data))
}

func Base64Decode(data string) string {
	b, err := base64.URLEncoding.DecodeString(data)
	if err != nil {
		return ""
	}
	return string(b)
}


// overwriting duplicate keys, you should handle that if there is a need
func MergeMaps(maps ...map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

func MergeMaps2() {
	log.Println(`started`)

	v := []map[string]interface{}{
		map[string]interface{}{
			`one`: 1,
			`two`: 2,
		},
		map[string]interface{}{
			`one`:   `I`,
			`three`: 3,
			`other`: `NAN`,
		},
		map[string]interface{}{
			`name`: `Bob`,
			`age`:  300,
		},
	}

	m := MergeMaps(v...)
	log.Println(m, len(m))
}

//BytesCombine 多个[]byte数组合并成一个[]byte
func BytesCombine(pBytes ...[]byte) []byte {
	len := len(pBytes)
	s := make([][]byte, len)
	for index := 0; index < len; index++ {
		s[index] = pBytes[index]
	}
	sep := []byte("")
	return bytes.Join(s, sep)
}


//BytesCombine 多个[]byte数组合并成一个[]byte
func BytesCombine2(pBytes ...[]byte) []byte {
	return bytes.Join(pBytes, []byte(""))
}
//求字符串的字符数，字符长度
func getWordNumOfString(str string) int64 {
	num := utf8.RuneCountInString(str)
	return int64(num)
}



