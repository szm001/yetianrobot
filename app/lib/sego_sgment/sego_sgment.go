package sego_sgment

import (
	"os"
	"fmt"
	"github.com/huichen/sego"
)

func Sego_sgment(filename string) {

	// 载入词典
	var segmenter sego.Segmenter
	segmenter.LoadDictionary("./data/dictionary.txt")

	//读取文件内容到buf中
	//filename := "../src/test/Text"
	fp, err := os.Open(filename)

	if err != nil {
		fmt.Println(filename, err)
		return
	}
	defer fp.Close()
	buf := make([]byte, 4096)
	n, _ := fp.Read(buf)
	if n == 0 {
		return
	}

	// 分词
	segments := segmenter.Segment(buf)
	for _,segment := range segments {
		token :=segment.Token().Text()
		fmt.Print(token,"=>")
		pos :=segment.Token().Pos()
		fmt.Println(pos)
	}
	// 处理分词结果
	// 支持普通模式和搜索模式两种分词，见utils.go代码中SegmentsToString函数的注释。
	// 如果需要词性标注，用SegmentsToString(segments, false)，更多参考utils.go文件
	//output := sego.SegmentsToSlice(segments, false)
	//输出分词后的成语
	//for _, v := range output {
	//	if len(v) == 12 {
	//		fmt.Println(v)
	//	}
	//}

	output := sego.SegmentsToString(segments, false)
	fmt.Println(output)
}
