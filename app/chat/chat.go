package chat

import (
	"bufio"
	"fmt"
	//"github.com/go-ego/gse"
	"github.com/huichen/sego"
	"math/rand"
	"os"
	"robot/app/entity"
	"robot/app/topic"
	"strings"
)
type Token struct{
	Name   string
	Pos    string
}
type ChatLog struct{
	Id         string
	Who        string
	When       string
	What       string
	segments   []Token
}
type Chat struct{
	Id         string
	Name       string
	Ier        entity.I
	Youer      entity.You
	Youlist    []entity.You
	Iter       entity.It
	Itlist     []entity.It
	Topic      topic.Topic
	TopicList  []topic.Topic
	Log         []ChatLog
}

func Initialize(args map[string]interface{})(chatroom Chat){
	if _, ok := args["ier"]; ok {
		// 存在
		chatroom.Ier = args["ier"].(entity.I)
	}
	if _, ok := args["youer"]; ok {
		// 存在
		chatroom.Youer = args["youer"].(entity.You)
	}else{
		//你，对话对象
		chatroom.Youer = entity.InitializeYou("you11111111")
		chatroom.Youlist = append(chatroom.Youlist, chatroom.Youer)
	}
	//他，谈话内容对象
	if _, ok := args["iter"]; ok {
		// 存在
		chatroom.Iter = args["Iter"].(entity.It)
	}else{
		//你，对话对象
		chatroom.Iter = entity.InitializeIt("it11111111")
		chatroom.Itlist = append(chatroom.Itlist, chatroom.Iter)
	}
	//话题
	if _, ok := args["topic"]; ok {
		// 存在
		chatroom.Topic = args["topic"].(topic.Topic)
	}else{
		//你，对话对象
		chatroom.Topic = topic.Initialize("topic11111111")
		chatroom.TopicList = append(chatroom.TopicList, chatroom.Topic)
	}
	return chatroom
}

func (chatroom Chat) Start(){
	// 载入词典
	//var gse_segmenter gse.Segmenter
	//gse_segmenter.LoadDict("./static/data/dictionary.txt")
	//gse_segmenter.LoadDict("zh,testdata/test_dict.txt,testdata/test_dict1.txt")
	// 载入词典
	var segmenter sego.Segmenter
	segmenter.LoadDictionary("./static/data/dictionary1.txt,./static/data/dictionary.txt")
	//var result string
	var content string
	//oba.Say("你好！我是"+oba.Name)
	reader := bufio.NewReader(os.Stdin)
	//var conteng_chan = make(chan string,1)
	//抢话筒
	//role := getSayRole()
	//if role=="i"{
	//	oba.Say_ok = true
	//}
	//if role=="you"{
	//	curr_you.Say_ok = true
	//}

	//go oba.DealWith(curr_you,conteng_chan)
	//go curr_you.Run()
	for {
		//fmt.Scan(&content)
		if chatroom.Youer.Say_ok==false{
			//time.Sleep(1*time.Second)
			//continue
		}
		fmt.Println("你说：")
		content,_ = reader.ReadString('\n')
		content = strings.TrimSpace(content)
		//conteng_chan <- content
		if content !=""{
			//oba.Say("oba嘴巴：你说的是："+content)
		}
		if  content =="走了\n" || content =="bye\n" || content =="byebye\n" || content =="再见\n" {
			os.Exit(0)
		}
		if  content =="走了" || content =="bye" || content =="byebye" || content =="再见" {
			os.Exit(0)
		}

		fmt.Println("分词1。。。：")
		// 分词1：字符分割
		//bytelistcontent := []rune(content)
		//for i:=0;i<len(bytelistcontent);i++{
		//	fmt.Print(string(bytelistcontent[i]),",")
		//}
		//fmt.Println("")

		// 分词2
		segments := segmenter.Segment([]byte(content))
		//fmt.Println(segments)
		//获得主谓宾，定状补
		wordInfo := chatroom.GetWordInfo(segments)
		println(wordInfo)

		//分词3
		//segment2 := gse_segmenter.Segment([]byte(content))
		//fmt.Println(gse.ToString(segment2))

		if chatroom.Youer.GetILikeName() ==""{
			//curr_you.Name.ILikeName[0].Name = content
			//oba.Say("你好："+curr_you.GetILikeName())
			//continue
		}
		//处理和回答
		//result = chatroom.Ier.Anwer(content)====
		//oba.Say_ok =true
		//curr_you.Say_ok = false
		//chatroom.Ier.Say(result)====
	}
}

//我的所有话题
func (chatroom Chat) GetAllTopic() (topiclist []topic.Topic){
	return chatroom.TopicList
}

func (chatroom Chat) getSayRole() string {
	var l []string =[]string{"i","you"}
	n := rand.Intn(2)
	return l[n]
}

//从语句中得到topic
func (chatroom Chat) GetTopic(tokenlist []sego.Token) (otopic topic.Topic){
	//判断说话对象
	oyou := chatroom.GetYou(tokenlist)
	otopic.Id = ""
	otopic.OYou = oyou
	return otopic
}

//从语句中得到You
func (chatroom Chat) GetYou(tokenlist []sego.Token) (oyou entity.You){
	//判断说话对象
	for _,v :=range tokenlist{
       if v.Pos()==""{

	   }
	}
	return oyou
}

