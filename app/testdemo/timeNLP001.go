package testdemo
import (
"log"
	"time"

	"github.com/bububa/TimeNLP"
)

func TestTimeNLP001() {
target := "大家好.大前天上午8点半开的会"
preferFuture := false
tn := timenlp.NewTimeNormalizer(preferFuture)
ret, err := tn.Parse(target,time.Now())
if err != nil {
log.Fatalln(err)
}
log.Printf("%+v\n", ret)
}