package database

import (
	"regexp"
	"robot/app/model"
	"strings"
	"io/ioutil"
	"unicode/utf8"
)


func Hanzi8300() {
	//读取汉字文件
	var wordlist []byte
	wordlist,_ = ioutil.ReadFile("E:\\szm\\go_project\\robot\\static\\data\\hanzi8300.txt")
	var wordstr string = string(wordlist)
	var mhanzi *model.HanziModel
	var id int64
	var strlen int64
	var strokeslist map[string]int64 = map[string]int64{
		"一":1,
		"二":2,
		"三":3,
		"四":4,
		"五":5,
		"六":6,
		"七":7,
		"八":8,
		"九":9,
		"十":10,
		"十一":11,
		"十二":12,
		"十三":13,
		"十四":14,
		"十五":15,
		"十六":16,
		"十七":17,
		"十八":18,
		"十九":19,
		"二十":20,
		"二十一":21,
		"二十二":22,
		"二十三":23,
		"二十四":24,
		"二十五":25,
		"二十六":26,
		"二十七":27,
		"二十八":28,
		"二十九":29,
	}
	arraylist := strings.Split(wordstr, "\n")
	var word_code string
	var word string
	var percent float32
	var strokes int64
	for _, row := range arraylist {
		mhanzi = new(model.HanziModel)
		row = strings.TrimSpace(row)
		if row ==""{
			continue
		}
		if strings.Contains(row,"级字表") {
			if strings.Contains(row,"一") {
				percent = 90
			}
			if strings.Contains(row,"二") {
				percent = 70
			}
			if strings.Contains(row,"三") {
				percent = 50
			}
			continue
		}

		if strings.Contains(row,"画") {
			match, _ := regexp.MatchString("\\d{4}", row) //有无4位连续的数字
			if !match {
				for strokesK, strokesV :=range strokeslist{
					if strings.Contains(row, strokesK) {
						strokes = strokesV
						break
					}
				}
				continue
			}
		}
		strlen = int64(utf8.RuneCountInString(row))
		if strlen == 4 {
			word_code = row
			word = ""
		}else{
			word_code = row[0:4]
			word = row[4:]
		}

		if word_code != "" {
			word_code = strings.TrimSpace(word_code)
			mhanzi.Word = word
			mhanzi.Word_code = word_code
			mhanzi.Percent = percent
			mhanzi.Strokes = strokes
			//是否存在：
			ishas := mhanzi.Ishas(word_code)
			if ishas > 0{
				r := mhanzi.Update("word_code=?",word_code)
				if r>=0{}
			}else{
				id = mhanzi.Save()
				if id>0{}
			}

		}
	}

}

