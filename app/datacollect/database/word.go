package database

import (
	"io/ioutil"
	"robot/app/model"
	"strings"
)
func Wordsave() {
	//读取字词文件
	wordbytelist,_ := ioutil.ReadFile("E:\\szm\\go_project\\robot\\static\\data\\dictionary_r.txt")
	wordstr := string(wordbytelist)
	arraylist := strings.Split(wordstr, "\n")
	for _, row := range arraylist {
		row                   = strings.TrimSpace(row)
		if row ==""{
			continue
		}
		pos                   := ""
		word                  := ""
		pos_first_cate        := ""
		pos_second_cate       := ""
		pos_third_cate        := ""
		pos_fourth_cate       := ""
		language              := "汉语"
		nation                := "汉族"
		country               := "中国"
		dialect               := "普通话"
		religion              := ""
		initial               := ""
		percent               := float32(80)
		situation             := "" //场合
		situation_status      := "" //场合状态：未开始，进行中，已结束
		times                 := "" //时代：古代，近代，现代
		gender                := "" //性别：男，女，通用
		agegroups             := "" //年龄段 幼儿，少年，青年，中年，老年
		marriage              := "" //婚否：已婚 未婚，离异',
		target                := "" //对象：丈夫，亲人，兄弟，姐妹，陌生人，敌人，朋友，同事，同伙，爱人，上司，儿女，下级，长辈，晚辈
		emotion               := "" //感情情感情绪：傲慢，谦虚，看不起，自卑
		occupation            := "" //职业或身份：学生，教师，律师，商业，企业主，军人，医生，小偷，逃兵，杀人犯，教父，村长，作家


		mPosword              := new(model.PoswordModel)
		rowlist               := strings.Split(row, " ")
		word                  = rowlist[0]
		pos                   = rowlist[2]
		mPos                  := new(model.PosModel)
		pos_map               := mPos.GetPosList(pos)
		pos_map_len           := len(pos_map)
		if pos_map_len>0{
			pos_first_cate    = pos_map[0]
		}
		if pos_map_len>1{
			pos_second_cate   = pos_map[1]
		}
		if pos_map_len>2{
			pos_third_cate    = pos_map[2]
		}
		if pos_map_len>3{
			pos_fourth_cate   = pos_map[3]
		}
		rowlist2              := strings.Split(rowlist[3], ",")
		for _,v := range rowlist2 {
			if v==""{
				continue
			}
			rowlist3 := strings.Split(v, ":")
			if rowlist3[0] == "语言"{
				language = rowlist3[1]
			}
			if rowlist3[0] == "时代"{
				times = rowlist3[1]
			}
			if rowlist3[0] == "性别"{
				gender = rowlist3[1]
			}
			if rowlist3[0] == "职业"{
				occupation = rowlist3[1]
			}
			if rowlist3[0] == "身份"{
				occupation = rowlist3[1]
			}
			if rowlist3[0] == "对象"{
				target = rowlist3[1]
			}
			if rowlist3[0] == "场合"{
				situation = rowlist3[1]
			}
			if rowlist3[0] == "感情"{
				emotion = rowlist3[1]
			}
			if rowlist3[0] == "年龄"{
				agegroups = rowlist3[1]
			}
			if rowlist3[0] == "婚否"{
				marriage = rowlist3[1]
			}
		}

		if word != "" {
			word                      = strings.TrimSpace(word)
			mPosword.Word             = word
			mPosword.Language         = language
			mPosword.Dialect          = dialect
			mPosword.Initial          = initial
			mPosword.Percent          = percent
			mPosword.Pos_first_cate   = pos_first_cate
			mPosword.Pos_second_cate  = pos_second_cate
			mPosword.Pos_third_cate   = pos_third_cate
			mPosword.Pos_fourth_cate  = pos_fourth_cate
			mPosword.Nation           = nation
			mPosword.Religion         = religion
			mPosword.Country          = country
			mPosword.Province         = ""
			mPosword.Region           = ""
			mPosword.County           = ""
			mPosword.Town             = ""
			mPosword.Village          = ""
			mPosword.Situation        = situation
			mPosword.Situation_status = situation_status
			mPosword.Times            = times
			mPosword.Gender           = gender
			mPosword.Agegroups        = agegroups
			mPosword.Marriage         = marriage
			mPosword.Target           = target
			mPosword.Emotion          = emotion
			mPosword.Occupation       = occupation
			mPosword.Sort             = 0
			mPosword.Status           = 0
			mPosword.Attribute        = ""
			mPosword.Note             = ""
			//是否存在：
			ishas := mPosword.Ishas(word)
			if ishas > 0{
				r := mPosword.Update("word=?",word)
				if r>=0{}
			}else{
				id := mPosword.Save()
				if id>0{}
			}

		}
	}

}

