package config

const (
	Appmode = "release" //debug or release
	Appport = ":9090"
	Appname = "同界机器人"
	Webtitle           = "同界机器人欢迎您！"
	Webkeywords        = "同界机器人,机器人"
	Webdescription     = "同界机器人是一个机器人"
	Domainname         = "www.cntoge.com"

	Port               = "80"
	Rooturl            = "/"
	Uploadurl          = "/upload/"
	Rootpath           = "e:\\go_preject\\robot\\"
	Uploadpath         = "e:\\go_preject\\robot\\upload\\"


	// 日志文件
	AppAccessLogName = "log/access.log"
	AppErrorLogName  = "log/error.log"
	AppGrpcLogName   = "log/grpc.log"

	// 系统告警邮箱信息
	SystemEmailUser = "xinliangnote@163.com"
	SystemEmailPass = "" //密码或授权码
	SystemEmailHost = "smtp.163.com"
	SystemEmailPort = 465

	// 告警接收人
	ErrorNotifyUser = "xinliangnote@163.com"

	// 告警开关 1=开通 -1=关闭
	ErrorNotifyOpen = -1

	// Jaeger 配置信息
	JaegerHostPort = "127.0.0.1:6831"

	// Jaeger 配置开关 1=开通 -1=关闭
	JaegerOpen = 1

	//redis
	RedisHost = "127.0.0.1"
	RedisPassword = ""
	RedisMaxIdle = 30
	RedisMaxActive = 30
	RedisIdleTimeout = 200
)