package topic

import (
	"robot/app/entity"
)
//主题
type Subject struct{
	Id string
	Starttime string
	Endtime string
	BigAttribute string
	MidAttribute string
	SmallAttribute string
}
//话题
type Topic struct{
	Id        string
	Otype     string  //'entity,it,you,i'
	Starttime string
	Endtime   string
	OEntity   entity.Entity
	Oit       entity.It
	OYou      entity.You
	OI        entity.I
	OSubject  []Subject
}

//话题的初始化
func Initialize(id string) (otopic Topic){
	otopic.Id                = id
	return otopic
}




