package nlp


type Pos2 struct {
	Code string `json:"code"`
	Name string `json:"name"`
	Note string `json:"note"`
}

var PosList2 []Pos2 = []Pos2{

	{
		Code: "ag",
		Name: "形语素",
		Note: "形容词性语素。形容词代码为 a，语素代码ｇ前面置以A。",
	},
	{
		Code: "a",
		Name: "形容词",
		Note: "取英语形容词 adjective的第1个字母。",
	},
	{
		Code: "ad",
		Name: "副形词",
		Note: "直接作状语的形容词。形容词代码 a和副词代码d并在一起。",
	},
	{
		Code: "an",
		Name: "名形词",
		Note: "具有名词功能的形容词。形容词代码 a和名词代码n并在一起。",
	},
	{
		Code: "b",
		Name: "区别词",
		Note: "取汉字“别”的声母。"},
	{
		Code: "c",
		Name: "连词",
		Note: "取英语连词 conjunction的第1个字母。",
	},
	{
		Code: "dg",
		Name: "副语素",
		Note: "副词性语素。副词代码为 d，语素代码ｇ前面置以D。",
	},
	{
		Code: "d",
		Name: "副词",
		Note: "取 adverb的第2个字母，因其第1个字母已用于形容词。",
	},
	{
		Code: "e",
		Name: "叹词",
		Note: "取英语叹词 exclamation的第1个字母。"},
	{
		Code: "f",
		Name: "方位词",
		Note: "取汉字“方”"},
	{
		Code: "g",
		Name: "语素",
		Note: "绝大多数语素都能作为合成词的“词根”，取汉字“根”的声母。"},
	{
		Code: "h",
		Name: "前接成分",
		Note: "取英语 head的第1个字母。",
	},
	{
		Code: "i",
		Name: "成语",
		Note: "取英语成语 idiom的第1个字母。",
	},
	{
		Code: "j",
		Name: "简称略语",
		Note: "取汉字“简”的声母。",
	},
	{
		Code: "k",
		Name: "后接成分",
		Note: "后接成分",
	},
	{
		Code: "l",
		Name: "习用语",
		Note: "习用语尚未成为成语，有点“临时性”，取“临”的声母。",
	},
	{
		Code: "m",
		Name: "数词",
		Note: "取英语 numeral的第3个字母，n，u已有他用。",
	},
	{
		Code: "ng",
		Name: "名语素",
		Note: "名词性语素。名词代码为 n，语素代码ｇ前面置以N。",
	},
	{
		Code: "n",
		Name: "名词",
		Note: "取英语名词 noun的第1个字母。",
	},
	{
		Code: "nr",
		Name: "人名",
		Note: "名词代码 n和“人(ren)”的声母并在一起。",
	},
	{
		Code: "ns",
		Name: "地名",
		Note: "名词代码 n和处所词代码s并在一起。",
	},
	{
		Code: "nt",
		Name: "机构团体",
		Note: "“团”的声母为 t，名词代码n和t并在一起。",
	},
	{
		Code: "nz",
		Name: "其他专名",
		Note: "“专”的声母的第 1个字母为z，名词代码n和z并在一起。",
	},
	{
		Code: "o",
		Name: "拟声词",
		Note: "取英语拟声词 onomatopoeia的第1个字母。",
	},
	{
		Code: "p",
		Name: "介词",
		Note: "取英语介词 prepositional的第1个字母。",
	},
	{
		Code: "q",
		Name: "量词",
		Note: "取英语 quantity的第1个字母。",
	},
	{
		Code: "r",
		Name: "代词",
		Note: "取英语代词 pronoun的第2个字母,因p已用于介词。",
	},
	{
		Code: "s",
		Name: "处所词",
		Note: "取英语 space的第1个字母。",
	},
	{
		Code: "tg",
		Name: "时语素",
		Note: "时间词性语素。时间词代码为 t,在语素的代码g前面置以T。",
	},
	{
		Code: "t",
		Name: "时间词",
		Note: "取英语 time的第1个字母。",
	},
	{
		Code: "u",
		Name: "助词",
		Note: "取英语助词 auxiliary",
	},
	{
		Code: "vg",
		Name: "动语素",
		Note: "动词性语素。动词代码为 v。在语素的代码g前面置以V。",
	},
	{
		Code: "v",
		Name: "动词",
		Note: "取英语动词 verb的第一个字母。",
	},
	{
		Code: "vd",
		Name: "副动词",
		Note: "直接作状语的动词。动词和副词的代码并在一起。",
	},
	{
		Code: "vn",
		Name: "名动词",
		Note: "指具有名词功能的动词。动词和名词的代码并在一起。",
	},
	{
		Code: "w",
		Name: "标点符号",
		Note: "标点符号",
	},
	{
		Code: "x",
		Name: "非语素字",
		Note: "非语素字只是一个符号，字母 x通常用于代表未知数、符号。",
	},
	{
		Code: "y",
		Name: "语气词",
		Note: "取汉字“语”的声母。",
	},
	{
		Code: "z",
		Name: "状态词",
		Note: "取汉字“状”的声母的前一个字母。",
	},
	{
		Code: "un",
		Name: "未知词",
		Note: "不可识别词及用户自定义词组。取英文Unkonwn首两个字母。(非北大标准，CSW分词中定义)",
	},
}

