package nlp

import (
	"robot/app/model"
	"strings"
)
type Pos struct {
	Code  string `json:"code"`
	Name  string `json:"name"`
	Note  string `json:"note"`
	Pcode string `json:"pcode"`
}


var PosList []Pos = []Pos{
	//1.名词(26个词性)
	{
		Code:  "n",
		Name:  "名词",
		Note:  "",
		Pcode: "",
	},
	{
		Code:  "nr",
		Name:  "人名",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nrj",
		Name: "日语人名",
		Note:  "",
		Pcode: "nr",
	},
	{
		Code: "nrf",
		Name: "音译人名",
		Note:  "",
		Pcode: "nr",
	},
	{
		Code: "ns",
		Name: "地名",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nsf",
		Name: "音译地名",
		Note:  "",
		Pcode: "ns",
	},
	{
		Code: "nt",
		Name: "机构团体名",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "ntc",
		Name: "公司名",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "ntcf",
		Name: "工厂",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "ntcb",
		Name: "银行",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "ntch",
		Name: "酒店宾馆",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "nto",
		Name: "政府机构",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "ntu",
		Name: "大学",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "nts",
		Name: "中小学",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "nth",
		Name: "医院",
		Note:  "",
		Pcode: "nt",
	},
	{
		Code: "nh",
		Name: "医药疾病等健康相关名词",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nhm",
		Name: "药品",
		Note:  "",
		Pcode: "nh",
	},
	{
		Code: "nhd",
		Name: "疾病",
		Note:  "",
		Pcode: "nh",
	},
	{
		Code: "nn",
		Name: "工作相关名词",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nnt",
		Name: "职务职称",
		Note:  "",
		Pcode: "nn",
	},
	{
		Code: "nnd",
		Name: "职业",
		Note:  "",
		Pcode: "nn",
	},
	{
		Code: "ng",
		Name: "名词性语素",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "ni",
		Name: "机构相关（不是独立机构名）",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nic",
		Name: "下属机构",
		Note:  "",
		Pcode: "ni",
	},
	{
		Code: "nis",
		Name: "机构后缀",
		Note:  "",
		Pcode: "ni",
	},
	{
		Code: "nm",
		Name: "物品名",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nmc",
		Name: "化学品名",
		Note:  "",
		Pcode: "nm",
	},
	{
		Code: "nb",
		Name: "生物名",
		Note:  "",
		Pcode: "n",
	},
	{
		Code: "nba",
		Name: "动物名",
		Note:  "",
		Pcode: "nb",
	},
	{
		Code: "nbp",
		Name: "植物名",
		Note:  "",
		Pcode: "nb",
	},
	{
		Code: "nz",
		Name: "其他专名",
		Note:  "",
		Pcode: "n",
	},
	//2.学术词(8个词性)
	{
		Code: "g",
		Name: "学术词汇",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "gm",
		Name: "数学相关词汇",
		Note:  "",
		Pcode: "g",
	},
	{
		Code: "gp",
		Name: "物理相关词汇",
		Note:  "",
		Pcode: "g",
	},
	{
		Code: "gc",
		Name: "化学相关词汇",
		Note:  "",
		Pcode: "g",
	},
	{
		Code: "gb",
		Name: "生物相关词汇",
		Note:  "",
		Pcode: "g",
	},
	{
		Code: "gbc",
		Name: "生物类别",
		Note:  "",
		Pcode: "gb",
	},
	{
		Code: "gg",
		Name: "地理地质相关词汇",
		Note:  "",
		Pcode: "g",
	},
	{
		Code: "gi",
		Name: "计算机相关词汇",
		Note:  "",
		Pcode: "g",
	},
	//3.简称省略语(1个一类，北大)
	{
		Code: "j",
		Name: "简称略语",
		Note:  "",
		Pcode: "",
	},
	//4.成语(1个一类，北大)
	{
		Code: "i",
		Name: "成语",
		Note:  "",
		Pcode: "",
	},
	//5.习用语(1个一类，北大)
	{
		Code: "l",
		Name: "习用语",
		Note:  "",
		Pcode: "",
	},
	//6.时间词(1个一类，1个二类)
	{
		Code: "t",
		Name: "时间词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "tg",
		Name: "时间词性语素",
		Note:  "",
		Pcode: "t",
	},
	//7.处所词(1个一类)
	{
		Code: "s",
		Name: "处所词",
		Note:  "",
		Pcode: "",
	},
	//8.方位词(1个一类)
	{
		Code: "f",
		Name: "方位词",
		Note:  "",
		Pcode: "",
	},
	//9.动词(1个一类，9个二类)
	{
		Code: "v",
		Name: "动词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "vd",
		Name: "副动词",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vn",
		Name: "名动词",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vshi",
		Name: "动词“是”",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vyou",
		Name: "动词“有”",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vf",
		Name: "趋向动词",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vx",
		Name: "形式动词",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vi",
		Name: "不及物动词（内动词",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vl",
		Name: "动词性惯用语",
		Note:  "",
		Pcode: "v",
	},
	{
		Code: "vg",
		Name: "动词性语素",
		Note:  "",
		Pcode: "v",
	},
	//10.形容词(1个一类，4个二类)
	{
		Code: "a",
		Name: "形容词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "ad",
		Name: "副形词",
		Note:  "",
		Pcode: "a",
	},
	{
		Code: "an",
		Name: "名形词",
		Note:  "",
		Pcode: "a",
	},
	{
		Code: "ag",
		Name: "形容词性语素",
		Note:  "",
		Pcode: "a",
	},
	{
		Code: "al",
		Name: "形容词性惯用语",
		Note:  "",
		Pcode: "a",
	},
	//11.区别词(1个一类，2个二类)
	{
		Code: "b",
		Name: "区别词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "bl",
		Name: "区别词性惯用语",
		Note:  "",
		Pcode: "b",
	},
	{
		Code: "bg",
		Name: "区别词语素",
		Note:  "",
		Pcode: "b",
	},
	//12.状态词(1个一类)
	{
		Code: "z",
		Name: "状态词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "zg",
		Name: "状态词语素",
		Note:  "",
		Pcode: "",
	},
	//13.代词(1个一类，4个二类，6个三类)
	{
		Code: "r",
		Name: "代词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "rw",
		Name: "物主代词",
		Note:  "",
		Pcode: "r",
	},
	{
		Code: "rwn",
		Name: "名词性物主代词",
		Note:  "名词性物主代词(mine, yours, his, hers, its, ours, theirs)。名词性物主代词则不能用作定语，但可以用作主语、宾语、表语、连用of作定语。",
		Pcode: "rw",
	},
	{
		Code: "rwa",
		Name: "形容词性物主代词",
		Note:  "形容词性物主代词(my, your, his, her, its, our, their)，形容词性物主代词在句只用作定语；",
		Pcode: "rw",
	},
	{
		Code: "rr",
		Name: "人称代词",
		Note:  "",
		Pcode: "r",
	},
	{
		Code: "rr1",
		Name: "第一人称代词",
		Note:  "",
		Pcode: "rr",
	},
	{
		Code: "rr2",
		Name: "第二人称代词",
		Note:  "",
		Pcode: "rr",
	},
	{
		Code: "rr3",
		Name: "第三人称代词",
		Note:  "",
		Pcode: "rr",
	},
	{
		Code: "rr4",
		Name: "旁称人称代词",
		Note:  "",
		Pcode: "rr",
	},
	{
		Code: "rz",
		Name: "指示代词",
		Note:  "",
		Pcode: "r",
	},
	{
		Code: "rzt",
		Name: "时间指示代词",
		Note:  "",
		Pcode: "rz",
	},
	{
		Code: "rzs",
		Name: "处所指示代词",
		Note:  "",
		Pcode: "rz",
	},
	{
		Code: "rzv",
		Name: "谓词性指示代词",
		Note:  "",
		Pcode: "rz",
	},
	{
		Code: "ry",
		Name: "疑问代词",
		Note:  "",
		Pcode: "r",
	},
	{
		Code: "ryt",
		Name: "时间疑问代词",
		Note:  "",
		Pcode: "ry",
	},
	{
		Code: "rys",
		Name: "处所疑问代词",
		Note:  "",
		Pcode: "ry",
	},
	{
		Code: "ryv",
		Name: "谓词性疑问代词",
		Note:  "",
		Pcode: "ry",
	},
	{
		Code: "rg",
		Name: "代词性语素",
		Note:  "",
		Pcode: "r",
	},
	//14.数词(1个一类，1个二类)
	{
		Code: "m",
		Name: "数词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "mq",
		Name: "数量词",
		Note:  "",
		Pcode: "m",
	},
	//15.量词(1个一类，2个二类)
	{
		Code: "q",
		Name: "量词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "qv",
		Name: "动量词",
		Note:  "",
		Pcode: "q",
	},
	{
		Code: "qt",
		Name: "时量词",
		Note:  "",
		Pcode: "q",
	},
	//16.副词(1个一类)
	{
		Code: "d",
		Name: "副词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "dc",
		Name: "程度副词",
		Note:  "",
		Pcode: "d",
	},
	{
		Code: "df",
		Name: "否定副词",
		Note:  "",
		Pcode: "d",
	},
	{
		Code: "dg",
		Name: "副语素",
		Note: "副词性语素。副词代码为 d，语素代码ｇ前面置以D。",
		Pcode: "d",
	},
	//17.介词(1个一类，2个二类)
	{
		Code: "p",
		Name: "介词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "pba",
		Name: "介词“把”",
		Note:  "",
		Pcode: "p",
	},
	{
		Code: "pbei",
		Name: "介词“被”",
		Note:  "",
		Pcode: "p",
	},
	//18.连词(1个一类，1个二类)
	{
		Code: "c",
		Name: "连词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "cc",
		Name: "并列连词",
		Note:  "",
		Pcode: "c",
	},
	//19.助词(1个一类，15个二类)
	{
		Code: "u",
		Name: "助词",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "uzhe",
		Name: "着",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "ule",
		Name: "了 喽",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "uguo",
		Name: "过",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "ude1",
		Name: "的 底",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "ude2",
		Name: "地",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "ude3",
		Name: "得",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "usuo",
		Name: "所",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "udeng",
		Name: "等 等等 云云",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "uyy",
		Name: "一样 一般 似的 般",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "udh",
		Name: "的话",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "uls",
		Name: "来讲 来说 而言 说来",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "uzhi",
		Name: "之",
		Note:  "",
		Pcode: "u",
	},
	{
		Code: "ulian",
		Name: "连（“连小学生都会”）",
		Note:  "",
		Pcode: "u",
	},
	//20.叹词(1个一类)
	{
		Code: "e",
		Name: "叹词",
		Note:  "",
		Pcode: "",
	},
	//21.语气词(1个一类)
	{
		Code: "y",
		Name: "语气词(delete yg)",
		Note:  "",
		Pcode: "",
	},
	//22.拟声词(1个一类)
	{
		Code: "o",
		Name: "拟声词",
		Note:  "",
		Pcode: "",
	},
	//23.前缀(1个一类)
	{
		Code: "h",
		Name: "前缀",
		Note:  "",
		Pcode: "",
	},
	//24.后缀(1个一类)
	{
		Code: "k",
		Name: "后缀",
		Note:  "",
		Pcode: "",
	},
	//25.字符串(1个一类，2个二类)
	{
		Code: "x",
		Name: "字符串",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "xx",
		Name: "非语素字",
		Note:  "",
		Pcode: "x",
	},
	{
		Code: "xu",
		Name: "网址URL",
		Note:  "",
		Pcode: "x",
	},
	//26.标点符号(1个一类，16个二类)
	{
		Code: "w",
		Name: "标点符号",
		Note:  "",
		Pcode: "",
	},
	{
		Code: "wkz",
		Name: "左括号，全角：（〔［｛《【〖〈  半角：([{<",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wky",
		Name: "右括号，全角：）〕］｝》】〗〉半角：)]{>",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wyz",
		Name: "左引号，全角：“‘『",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wyy",
		Name: "右引号，全角：”’』",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wj",
		Name: "句号，全角：。",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "ww",
		Name: "问号，全角：？半角：?",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wt",
		Name: "叹号，全角：！半角：!",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wd",
		Name: "逗号，全角：，半角：,",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wf",
		Name: "分号，全角：；半角：;",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wn",
		Name: "顿号，全角：、",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wm",
		Name: "冒号，全角：：半角：:",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "ws",
		Name: "省略号，全角：„„  „",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wp",
		Name: "破折号，全角：—— －－ ——－  半角： --- ----",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wb",
		Name: "百分号千分号，全角：％   半角：%",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "wh",
		Name: "单位符号，全角：￥ ＄ ￡ ° ℃ 半角：$",
		Note:  "",
		Pcode: "w",
	},
	{
		Code: "un",
		Name: "未知词",
		Note: "不可识别词及用户自定义词组。取英文Unkonwn首两个字母。(非北大标准，CSW分词中定义)",
		Pcode: "",
	},
}

func GetPos(Code string) (p Pos) {
	var notPos Pos
	for _, v := range PosList {
		if v.Code == Code {
			p = v
		}
		if v.Code == "un" {
			notPos = v
		}
	}
	return notPos
}




func SavePOS() {
	var mpos *model.PosModel
	var id int64
	for _, v := range PosList {
		mpos = new(model.PosModel)
		if v.Code != "" {
			v.Code = strings.TrimSpace(v.Code)
			mpos.Name = v.Name
			mpos.Pcode = v.Pcode
			mpos.Word = v.Code
			mpos.Note = v.Note
			//是否存在：
			ishas := mpos.Ishas(v.Code)
			if ishas > 0{
				//r := mpos.Update("word=?",v.Code)
				//if r>=0{}
			}else{
				id = mpos.Save()
				if id>0{}
			}

		}
	}

}
